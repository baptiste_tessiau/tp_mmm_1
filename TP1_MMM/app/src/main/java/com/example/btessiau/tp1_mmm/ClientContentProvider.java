package com.example.btessiau.tp1_mmm;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by btessiau on 18/01/16.
 */
public class ClientContentProvider extends ContentProvider {
    private DAO datasource;


    public static final String _ID = "_ID";
    public static final String CLIENT_NOM = "nom";
    public static final String CLIENT_PRENOM = "prenom";
    public static final String CLIENT_DATE = "date";
    public static final String CLIENT_VILLE = "ville";

    // This must be the same as what as specified as the Content Provider authority
    // in the manifest file.
    static final String AUTHORITY = "clientcontentprovider";

    public static final String PROVIDER_NAME = "clientcontentprovider";

    public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME);

    @Override
    public boolean onCreate() {
        datasource = new DAO(this.getContext());
        datasource.open();

        return true;
    }

    @Override
    public String getType(Uri uri) {
        return ContentResolver.CURSOR_DIR_BASE_TYPE + '/' + "com.example.btessiau";
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        MatrixCursor c = new MatrixCursor(new String[] {
                _ID,
                CLIENT_NOM,
                CLIENT_PRENOM,
                CLIENT_DATE,
                CLIENT_VILLE
        });

        int row_index = 0;
        List<Client> data = datasource.getAllClients();

        // Add x-axis data
        for (int i=0; i< data.size(); i++) {


            c.newRow()
                    .add( data.get(i).getId() )
                    .add( data.get(i).getNom() )
                    .add( data.get(i).getPrenom() )
                    .add( data.get(i).getDate() )
                    .add( data.get(i).getVille() );

            row_index++;
        }

        return c;
    }

    @Override
    public int update(Uri uri, ContentValues contentvalues, String s, String[] as) {
        throw new UnsupportedOperationException(" To be implemented");
    }


    @Override
    public Uri insert(Uri uri, ContentValues contentvalues) {
        datasource.createClient(contentvalues.getAsString("nom"),
                contentvalues.getAsString("prenom"),
                contentvalues.getAsString("date"),
                contentvalues.getAsString("ville"));

        /*data.add(new Client(contentvalues.getAsString("nom"),
                contentvalues.getAsString("prenom"),
                contentvalues.getAsString("date"),
                contentvalues.getAsString("ville")));*/
        return uri;
    }

    @Override
    public int delete(Uri uri, String s, String[] as) {
        datasource.close();
        return 0;
    }
}
