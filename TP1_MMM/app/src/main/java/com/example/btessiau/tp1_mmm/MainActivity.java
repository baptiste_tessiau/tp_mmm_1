package com.example.btessiau.tp1_mmm;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_reset) {
            resetMethod();
            return true;
        } else if (id == R.id.action_telephone) {
            telephoneMethod();
            return true;
        } else if (id == R.id.action_browser) {
            browserMethod();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    public void ValiderMethod(View v) {
        LinearLayout textLayout = (LinearLayout) findViewById(R.id.textLayout);

        for(int i = 0; i < textLayout.getChildCount(); i++) {
            if (textLayout.getChildAt(i).getId() != R.id.departementLabel && textLayout.getChildAt(i).getId() != R.id.departement) {
                validateView((EditText) textLayout.getChildAt(i));
            }
        }
    }

    private void validateView(View v) {
        switch (v.getId()) {
            case R.id.nom:
                Toast.makeText(getApplicationContext(),
                        ((EditText) v).getText(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.prenom:
                Toast.makeText(getApplicationContext(),
                        ((EditText) v).getText(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.date:
                Toast.makeText(getApplicationContext(),
                        ((EditText) v).getText(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.ville:
                Toast.makeText(getApplicationContext(),
                        ((EditText) v).getText(), Toast.LENGTH_SHORT).show();
                break;
        }
    }*/

    /*
    public void ValiderMethodIntoSecondActivity(View v) {
        Intent intent = new Intent(this, Valider.class);
        LinearLayout textLayout = (LinearLayout) findViewById(R.id.textLayout);

        for(int i = 0; i < textLayout.getChildCount(); i++) {
            if (textLayout.getChildAt(i).getId() != R.id.departementLabel && textLayout.getChildAt(i).getId() != R.id.departement) {
                validateViewForSecondActivity(textLayout.getChildAt(i), intent);
            }
        }
        startActivity(intent);
    }

    private void validateViewForSecondActivity(View v, Intent intent) {
        switch (v.getId()) {
            case R.id.nom:
                intent.putExtra("nom", ((EditText) v).getText().toString());
                break;
            case R.id.prenom:
                intent.putExtra("prenom", ((EditText) v).getText().toString());
                break;
            case R.id.date:
                intent.putExtra("date", ((EditText) v).getText().toString());
                break;
            case R.id.ville:
                intent.putExtra("ville", ((EditText) v).getText().toString());
                break;
        }
    }*/

    public void ValiderMethodIntoSecondActivityWithParcelable(View v) {
        //Intent intent = new Intent(this, Valider.class);
        Intent intent = new Intent(this, NouveauClient.class);

        String nom = ((EditText)findViewById(R.id.nom)).getText().toString();
        String prenom = ((EditText)findViewById(R.id.prenom)).getText().toString();
        String date = ((EditText)findViewById(R.id.date)).getText().toString();
        String ville = ((EditText)findViewById(R.id.ville)).getText().toString();

        if (!nom.isEmpty() && !prenom.isEmpty() && !date.isEmpty() && !ville.isEmpty() ) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("nom",nom);
            contentValues.put("prenom",prenom);
            contentValues.put("date",date);
            contentValues.put("ville", ville);

            getContentResolver().insert(ClientContentProvider.CONTENT_URI,contentValues);
        }

        /*Datas datas = new Datas(((EditText)findViewById(R.id.nom)).getText().toString()
                ,((EditText)findViewById(R.id.prenom)).getText().toString()
                ,((EditText)findViewById(R.id.date)).getText().toString()
                ,((EditText)findViewById(R.id.ville)).getText().toString());

        intent.putExtra("datas",datas);*/

        setResult(RESULT_OK, intent);
        finish();
    }

    private void resetMethod() {
        LinearLayout textLayout = (LinearLayout) findViewById(R.id.textLayout);

        for(int i = 0; i < textLayout.getChildCount(); i++) {
            if (textLayout.getChildAt(i).getId() != R.id.departementLabel && textLayout.getChildAt(i).getId() != R.id.departement) {
                resetView(textLayout.getChildAt(i));
            }
        }
    }

    private void resetView(View v) {
            switch (v.getId()) {
                case R.id.nom:
                    ((EditText) v).setText("");
                    break;
                case R.id.prenom:
                    ((EditText) v).setText("");
                    break;
                case R.id.date:
                    ((EditText) v).setText("");
                    break;
                case R.id.ville:
                    ((EditText) v).setText("");
                    break;
            }
    }

    private boolean telephoneActivated = false;
    private void telephoneMethod() {
        if (!telephoneActivated) {
            EditText telephone = new EditText(this);
            telephone.setText("");
            telephone.setHint("Téléphone");
            telephone.setInputType(3);
            LinearLayout textLayout = (LinearLayout) findViewById(R.id.textLayout);
            textLayout.addView(telephone);
            telephoneActivated = true;
        }
    }

    private void browserMethod() {
        EditText textLayout = (EditText) findViewById(R.id.ville);

        Intent intent = new Intent(Intent.ACTION_VIEW,
            Uri.parse("http://fr.wikipedia.org/wiki/" + textLayout.getText()));
        startActivity(intent);
    }
}
