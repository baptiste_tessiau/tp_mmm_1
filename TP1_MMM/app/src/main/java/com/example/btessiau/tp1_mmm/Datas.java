package com.example.btessiau.tp1_mmm;

import android.os.Parcel;
import android.os.Parcelable;

public class Datas implements Parcelable {
    private String mNom;
    private String mPrenom;
    private String mDate;
    private String mVille;

    public Datas(String nom, String prenom,
                String date, String ville)
    {
        super();
        this.mNom = nom;
        this.mPrenom = prenom;
        this.mDate = date;
        this.mVille = ville;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mNom);
        dest.writeString(mPrenom);
        dest.writeString(mDate);
        dest.writeString(mVille);
    }

    public static final Parcelable.Creator<Datas> CREATOR
            = new Parcelable.Creator<Datas>() {
        public Datas createFromParcel(Parcel source) {

            return new Datas(source);
        }

        public Datas[] newArray(int size) {
            return new Datas[size];
        }
    };

    private Datas(Parcel in) {
        this.mNom = in.readString();
        this.mPrenom = in.readString();
        this.mDate = in.readString();
        this.mVille = in.readString();
    }

    public String getmNom() {
        return mNom;
    }

    public String getmPrenom() {
        return mPrenom;
    }

    public String getmDate() {
        return mDate;
    }

    public String getmVille() {
        return mVille;
    }
}