package com.example.btessiau.tp1_mmm;

/**
 * Created by btessiau on 10/02/16.
 */
public class Client {
    private long id;

    public String nom;
    public String prenom;
    public String date;
    public String ville;

    public Client() {
    }

    public Client(String string, String string2, String string3, String string4) {
        nom = string;
        prenom = string2;
        date = string3;
        ville = string4;
    }

    //	getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return nom+ ";" + prenom + ";" + date + ";" + ville;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}