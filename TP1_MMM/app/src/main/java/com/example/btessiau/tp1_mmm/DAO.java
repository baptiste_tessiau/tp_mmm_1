package com.example.btessiau.tp1_mmm;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by btessiau on 10/02/16.
 */
public class DAO {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_NOM,
            MySQLiteHelper.COLUMN_PRENOM,
            MySQLiteHelper.COLUMN_DATE,
            MySQLiteHelper.COLUMN_VILLE};

    public DAO(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Client createClient(String nom,String prenom, String date, String ville) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_NOM, nom);
        values.put(MySQLiteHelper.COLUMN_PRENOM, prenom);
        values.put(MySQLiteHelper.COLUMN_DATE, date);
        values.put(MySQLiteHelper.COLUMN_VILLE, ville);
        long insertId = database.insert(MySQLiteHelper.TABLE_CLIENTS, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_CLIENTS,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Client newComment = cursorToClient(cursor);
        cursor.close();
        return newComment;
    }

    public void deleteClient(Client client) {
        long id = client.getId();
        System.out.println("Client deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_CLIENTS, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Client> getAllClients() {
        List<Client> clients = new ArrayList<Client>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_CLIENTS,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Client client = cursorToClient(cursor);
            clients.add(client);
            cursor.moveToNext();
        }
        // Make sure to close the cursor
        cursor.close();
        return clients;
    }

    private Client cursorToClient(Cursor cursor) {
        Client client = new Client();
        client.setId(cursor.getLong(0));
        client.setNom(cursor.getString(1));
        client.setPrenom(cursor.getString(2));
        client.setDate(cursor.getString(3));
        client.setVille(cursor.getString(4));
        return client;
    }
}