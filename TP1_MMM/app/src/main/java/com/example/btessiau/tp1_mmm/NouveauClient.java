package com.example.btessiau.tp1_mmm;


import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class NouveauClient extends AppCompatActivity {
    private ListView maListViewPerso;
    private SimpleAdapter mListAdapter;
    private ArrayList<HashMap<String, String>> listItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nouveau_client);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        //Récupération de la listview créée dans le fichier main.xml
        maListViewPerso = (ListView) findViewById(R.id.listviewperso);

        //Création de la ArrayList qui nous permettra de remplire la listView
        listItem = new ArrayList<HashMap<String, String>>();

        //On déclare la HashMap qui contiendra les informations pour un item
        /*HashMap<String, String> map;

        //Création d'une HashMap pour insérer les informations du premier item de notre listView
        map = new HashMap<String, String>();
        map.put("nom", "Tessiau");
        map.put("prenom", "Baptiste");
        map.put("date", "17/07/1992");
        map.put("ville", "Rennes");
        //enfin on ajoute cette hashMap dans la arrayList
        listItem.add(map);

        //On refait la manip plusieurs fois avec des données différentes pour former les items de notre ListView

        map = new HashMap<String, String>();
        map.put("nom", "Lebastard");
        map.put("prenom", "Maxime");
        map.put("date", "01/01/1993");
        map.put("ville", "Rennes");
        listItem.add(map);

        map = new HashMap<String, String>();
        map.put("nom", "Gestin");
        map.put("prenom", "Gweltaz");
        map.put("date", "24/12/1991");
        map.put("ville", "Pabu");
        listItem.add(map);*/

        //Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (listItem) dans la vue affichageitem
        mListAdapter = new SimpleAdapter (this.getBaseContext(), listItem, R.layout.listviewitem,
                new String[] {"nom", "prenom", "date", "ville"}, new int[] {R.id.nom, R.id.prenom, R.id.date, R.id.ville});

        //On attribue à notre listView l'adapter que l'on vient de créer
        maListViewPerso.setAdapter(mListAdapter);



        maListViewPerso.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                //on récupère la HashMap contenant les infos de notre item (titre, description, img)
                HashMap<String, String> map = (HashMap<String, String>) maListViewPerso.getItemAtPosition(position);
                //on créer une boite de dialogue
                AlertDialog.Builder adb = new AlertDialog.Builder(NouveauClient.this);
                //on attribut un titre à notre boite de dialogue
                adb.setTitle("Sélection Item");
                //on insère un message à notre boite de dialogue, et ici on affiche le titre de l'item cliqué
                adb.setMessage("Votre choix : "+map.get("nom"));
                //on indique que l'on veut le bouton ok à notre boite de dialogue
                adb.setPositiveButton("Ok", null);
                //on affiche la boite de dialogue
                adb.show();
            }
        });


        /*
        //On ajoute un listener à notre Adapter
        mListAdapter.registerDataSetObserver(new DataSetObserver() {
        	public void onChanged() {

        	}
        	public void onInvalidated() {

        	}
		});
		*/

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        updateList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                /*System.out.print(data);
                Datas datas = data.getExtras().getParcelable("datas");
                addItem(datas.getmNom(),datas.getmPrenom(),datas.getmDate(),datas.getmVille());
                */
                updateList();
            }
        }
    }

    private void updateList() {
        listItem.clear();
        Cursor cursor = getContentResolver().query(ClientContentProvider.CONTENT_URI, null, null, null, null);
        // 3) parse the data
        if (cursor.moveToFirst()) {
            do {

                addItem(cursor.getString(cursor.getColumnIndex(
                                ClientContentProvider.CLIENT_NOM)),
                        cursor.getString(cursor.getColumnIndex(
                                ClientContentProvider.CLIENT_PRENOM)),
                        cursor.getString(cursor.getColumnIndex(
                                ClientContentProvider.CLIENT_DATE)),
                        cursor.getString(cursor.getColumnIndex(
                                ClientContentProvider.CLIENT_VILLE)));


            } while (cursor.moveToNext());
        }
    }

    static final int PICK_CONTACT_REQUEST = 1;  // The request code

    public void newClientMethod(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivityForResult(intent, PICK_CONTACT_REQUEST);
    }

    private void addItem(String nom, String prenom, String date, String ville) {
        HashMap<String, String> map;
        map = new HashMap<String, String>();
        map.put("nom", nom);
        map.put("prenom", prenom);
        map.put("date", date);
        map.put("ville", ville);
        listItem.add(map);
        mListAdapter.notifyDataSetChanged();
    }
}
