package com.example.btessiau.tp1_mmm;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Valider extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valider);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        initiate();
    }

    private void initiate() {
        Intent intent = getIntent();

        /*
        String nom = intent.getStringExtra("nom");
        String prenom = intent.getStringExtra("prenom");
        String date = intent.getStringExtra("date");
        String ville = intent.getStringExtra("ville");
        */
        Datas datas = intent.getExtras().getParcelable("datas");


        LinearLayout textLayoutValider = (LinearLayout) findViewById(R.id.textLayoutValider);
        TextView nomText = (TextView)textLayoutValider.findViewById(R.id.nom);
        TextView prenomText = (TextView)textLayoutValider.findViewById(R.id.prenom);
        TextView dateText = (TextView)textLayoutValider.findViewById(R.id.date);
        TextView villeText = (TextView)textLayoutValider.findViewById(R.id.ville);

        /*
        nomText.setText("Nom : " + nom);
        prenomText.setText("Prénom : " + prenom);
        dateText.setText("Date de naissance : " + date);
        villeText.setText("Ville de naissance : " + ville);
        */

        nomText.setText("Nom : " + datas.getmNom());
        prenomText.setText("Prénom : " + datas.getmPrenom());
        dateText.setText("Date de naissance : " + datas.getmDate());
        villeText.setText("Ville de naissance : " + datas.getmVille());
    }

}
